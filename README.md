# README #

Install Google Chrome App from here: https://chrome.google.com/webstore/bfllijokfabcdciijaiionlacdfoleei?hl=en-GB&gl=IN

### What is this repository for? ###

* Contribute your ideas, themes, levels...
* 1.1

### Who do I talk to? ###

* Kulin Choksi <kulinchoksi@gmail.com>

## To DO ##
a) Make it social media enabled (send best scores)

b) Change/Improve look and feel - UI (may provide themes)

c) Give difficulty levels

d) Leaderboard for the 10 best scores with names and emails - just on the computer itself, nothing online

e) Tooltip for expressive UI

f) How-to-play-guide

g) Timer to make it more exciting

h) May implement jQuery or some library to optimize performance if complexity increases